package rasterops;

import imagedata.Image;
import io.vavr.collection.Stream;
import org.jetbrains.annotations.NotNull;

public class LineRendererNaive<T> implements LineRenderer<T> {

    @NotNull
    @Override
    public Image<T> render(
            @NotNull Image<T> image,
            double x1, double y1, double x2, double y2,
            @NotNull T value) {
        final double ix1 = (x1 + 1) * image.getWidth() / 2;
        final double iy1 = (-y1 + 1) * image.getHeight() / 2;
        final double ix2 = (x2 + 1) * image.getWidth() / 2;
        final double iy2 = (-y2 + 1) * image.getHeight() / 2;
        final double k = (iy2 - iy1) / (ix2 - ix1);
        final double q = iy1 - k * ix1;
        final int c1 = (int) ix1, c2 = (int) ix2;
        return Stream.rangeClosed(c1, c2).foldLeft(
                image,
                (currentImage, c) -> {
                    final double ix = c;
                    final double iy = k * ix + q;
                    final int r = (int) iy;
                    return currentImage.withValue(c, r, value);
                }
        );
    }
}
