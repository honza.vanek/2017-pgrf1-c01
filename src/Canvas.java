import imagedata.*;
import io.vavr.collection.Stream;
import objectdata.Cube;
import objectops.RenderWireframe;
import org.jetbrains.annotations.NotNull;
import rasterops.FloodFill4;
import rasterops.LineRenderer;
import rasterops.LineRendererNaive;
import transforms.Camera;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.function.Function;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */

/* terminy uloh
uloha 1: 26.12.2017
uloha 2: 31.12.2017
uloha 3: 7.1.2018
 */

public class Canvas {

	private final JFrame frame;
	private final JPanel panel;
	private final BufferedImage img;
	private @NotNull Image<Color> image;
	private final @NotNull Presenter<Color, Graphics> presenter;
	private final @NotNull LineRenderer<Color> lineRenderer;

	public Canvas(final int width, final int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
/*
		image = new ImageUgly<>(
			img,
			//Function<UglyPixelTYpe,Integer> toRGB, kde UglyPixelType = Integer
			Function.identity()
			,
			//Function<Integer, UglyPixelTYpe> toUgly, kde UglyPixelType = Integer
			Function.identity()
		);
		presenter = new PresenterUgly<>();
/*/
		image = new ImageVavr<>(width, height, new Color(0xffff0000));
		presenter = new PresenterUniversal<>(
				color -> color.getRGB()
		);
//*/

		lineRenderer = new LineRendererNaive<>();

		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	public void clear() {
		/*
		Graphics gr = img.getGraphics();
		gr.setColor(new Color(0x2f2f2f));
		gr.fillRect(0, 0, img.getWidth(), img.getHeight());
		*/
		image = image.cleared(new Color(0xff2f2f2f));
	}

	public void present(final Graphics graphics) {
//		graphics.drawImage(img, 0, 0, null);
		presenter.present(image, graphics);
	}

	public void draw() {
		clear();
		image = Stream.range(1, 10).foldLeft(image,
			(currentImage, i) -> currentImage.withValue(10,10 + i, new Color(0xff00ff00))
		);
		/*
		image = lineRenderer.render(
				image, -0.9, -0.8, 0.9, 0.8, new Color(0xffff00ff));
		image = lineRenderer.render(
				image, -0.9, 0.8, 0.9, -0.8, new Color(0xffff00ff));
		image = lineRenderer.render(
				image, -0.9, -0.2, 0.9, -0.2, new Color(0xffff00ff));
		image = new FloodFill4<Color>().fill(image,
				image.getWidth() / 2, image.getHeight() / 2 +5,
				new Color(0xffffff00),
				value -> value.getRGB() == 0xff2f2f2f
		);
		*/
		image =
				new RenderWireframe<Color>(lineRenderer)
						.render(image, new Cube(),
								new Camera()
										.withPosition(new Vec3D(5,3,2))
										.withAzimuth(Math.PI)
										.withZenith(-Math.atan(2.0 / 5.0))
										.getViewMatrix()
										.mul(new Mat4PerspRH(
												Math.PI / 3,
												image.getHeight() / (double) image.getWidth(),
												0.1, 1000
										)),
								new Color(1.0f, 1, 0));

	}

	public void start() {
		draw();
		panel.repaint();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Canvas(800, 600)::start);
	}

}